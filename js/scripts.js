/*
 author: babajide s adegbenro
 description: custom js script file for menu generation and scrollspy.
 version: 1.0
 */

/* scrollspy */
$(document).ready(function () {
    $("body").scrollspy({
        target: '.navbar',
        offset: 70
    })
});

/* menu generation */
$(function () {
    var data;
    /* retrieve data from the json file */
    $.getJSON('js/menu.json', function (data) {
        var getMenuItem = function (itemData) {
            var item = $("<li>", {class: itemData.status}).append(
                $("<a>", {
                    href: '#' + itemData.link,
                    html: itemData.name,
                    class: itemData.link_class,
                    'data-toggle': itemData.data_toggle,
                    role: itemData.role,
                    'aria-haspopup': itemData.aria_haspopup,
                    'aria-expanded': itemData.aria_expanded
                }).append(itemData.span_class)
            );
            if (itemData.sub) {
                var subList = $("<ul class='dropdown-menu'>");
                $.each(itemData.sub, function () {
                    subList.append(getMenuItem(this));
                });
                item.append(subList);
            }
            return item;
        };

        var $menu = $(".nav");
        $.each(data.menu, function () {
            $menu.append(
                getMenuItem(this)
            );
        });
    });
});